import copy

# Transforms a list of clauses into it's respective multi-encoding.
# A clause is a list of strings that represents the features of that clause.
#
# Args:
#   contrato: String[][]
#
# Return:
#   int[][]
#

def features_to_hot_encoding(contrato):
  n_clausulas = len(contrato)
  palabras = len(vocabulario)
  hot_encoded = np.zeros((n_clausulas,palabras))
  cont = 0
  keys = vocabulario.keys()
  for i in contrato:
    encode = np.zeros((palabras))
    for j in i:
      if j in keys:
        index = vocabulario[j]
        encode[index] = 1
    if(len(np.nonzero(encode)[0]) == 0 and len(i) > 0):
      pass
    else:
      hot_encoded[cont] = encode
      cont += 1
    
  while cont < n_clausulas:
    hot_encoded[cont] = np.zeros((palabras))
    cont += 1
  return hot_encoded

# Creates or expands a 3D matrix composed by each example to be taken by the ML model.
#
# Args:
#   m1: Matrix 1
#   m2: Matrix 2
#
# Returns:
#   A 3D matrix composed by m1 and m2
#
def stack_matrix_first_axis(m1, m2):
    assert m1.shape[1] == m2.shape[0], "Shape must be equal and was " + str(m1.shape[1]) + " and " + str(m2.shape[1]) 
    assert m1.shape[2] == m2.shape[1], "Shape must be equal and was " + str(m1.shape[1]) + " and " + str(m2.shape[1])
    return np.append(m1,np.array([m2]),axis=0)

def check_not_end(contrs):
  for i in contrs:
    for f in i:
      for j in f:
        if j == "fin del contrato": 
          return False
  return True


# Returns X and Y embebbing representation of a given contract.
# Input: A list that contains contracts, described by a list of different feature vectors of our vocabulary.
# Output: 
#        x,y matrix of encoded representation.
def contracts_to_embbebing(contrs_clean):
  
  contrs = contrs_clean
  
  assert len(contrs) != 0, "Length must be different to zero"
  assert isinstance(contrs[0], list), "Contracts must be a list"
  assert isinstance(contrs[0][0][0], str) and len(contrs[0][0][0]) > 1, "Clauses must be strings"
  assert check_not_end(contrs), "Contracts can not have end"
  
  contrs = copy.deepcopy(contrs_clean)
  
  maxlength = check_max_length(contrs)
  
  # to add the end
  maxlength = maxlength +1 
   
  # add fin and verify the size  
  for contr in contrs:
    contr.append(["fin del contrato"])
    while len(contr) < maxlength:
      contr.append([])
  
       
  m = len(contrs)
  
  #Creating X and Y 
  
  x = features_to_hot_encoding(contrs[0][:maxlength-1])
  
  x = np.expand_dims(x,axis=0)
  
  y = features_to_hot_encoding(contrs[0][1:maxlength])
  
  y = np.expand_dims(y,axis=0)

  
  for i in contrs[1:]:
    x = stack_matrix_first_axis(x,features_to_hot_encoding(i[:maxlength-1]))
    y = stack_matrix_first_axis(y,features_to_hot_encoding(i[1:maxlength]))
    
  return x,y
