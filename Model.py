# This sequence model is composed by two LSTM layers with Dropout among them,
#    and a final Dense layer using Sigmoid as final activation and Binary
#    CrossEntropy for Loss function. RMSprop is our optimizer.
# Args:
#   vocab_size: The number of features that describe a narrative intention
#               in a clause (Can be one or more)
# Returns:
#   Keras Model
#
#
def modelo(vocab_size):
  model = Sequential()
  model.add(LSTM(18, input_shape=(None,vocab_size), return_sequences=True))
  model.add(Dropout(0.2))
  model.add(LSTM(16, input_shape=(None,vocab_size), return_sequences=True))
  model.add(Dropout(0.4))
  model.add(Dense((vocab_size)))
  model.add(Activation('sigmoid'))
  optimizer = RMSprop(lr=0.01)
  model.compile(loss='binary_crossentropy', optimizer=optimizer, metrics=["accuracy"])
  return model
