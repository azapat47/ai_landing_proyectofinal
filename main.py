import numpy as np
import keras
from keras import Sequential
from keras import backend as K
from keras.callbacks import ModelCheckpoint
from keras.layers import Embedding, LSTM, Activation, Dense, Input, Dropout
from keras.optimizers import RMSprop
from matplotlib import pyplot as plt

# Draws with MatPlotLib the train and dev Loss curve.
#
# Args:
#  History: Is the output of the function train in Keras
#
def draw(history):
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Train / Test Performance')
    access = plt.gca()
    access.set_xlim(100)
    access.set_ylim(0,0.125)
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left')
    plt.grid()
    plt.show()
