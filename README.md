# ia_landing_lawyers

RNN model - Intelligente assistant in the creation of a contract.

Andrés Felipe Zapata Palacio
Juan David Arcila Moreno

## Software Dependencies ##
* Python 3.x
* TensorFlow 1.13.1
* Keras 2.2.4
* numpy

## Python enviroment

conda create -n <name_env>
conds activate <name_env>
pip install -r requirements.txt

## Note

The code included in this repository is not the final/complete/useful version of the code. 
In order to complaing the confidential terms of our contract with Cavelier Abogados, those versions 
are not avaible to the public.
