import numpy as np
import os
from keras.models import model_from_json
from keras.optimizers import RMSprop
import settings
import redis
import json
import time
import copy

# Loads the trained model from model.json and model.h5
def load_model():
    print("Loading Model From FS...")
    json_file = open('model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("model.h5")
    # evaluate loaded model on test data
    opti=RMSprop(lr=0.01)
    loaded_model.compile(loss='binary_crossentropy', optimizer=opti, metrics=['accuracy'])
    print("Done")
    return loaded_model

# Executes an active wait that consumes the data from Redis stored requests
# and returns the prediction as a response stored again in Redis
def main():
    model = load_model()
    set_vocabulario()

    db = redis.StrictRedis(host=settings.REDIS_HOST,
                  port=settings.REDIS_PORT, db=settings.REDIS_DB)
    while True:
      e = db.lpop(settings.QUEUE)
      if(e is not None):
         element = json.loads(e.decode('utf-8'))
         id = element["id"]
         contract = element["data"]
         x, y = contracts_to_embbebing([contract])
         out = model.predict(x)
         pred = out[:,-1]
         values = pred_to_map(pred)
         return_map = {"data":values}
         db.set(id, json.dumps(return_map)) 
      time.sleep(settings.SERVER_SLEEP)
